# Japanese translations for Debian release notes
# Debian リリースノートの日本語訳
# Noritada Kobayashi <nori1@dolphin.c.u-tokyo.ac.jp>, 2006.
# Hisashi Morita, 2009.
# Hideki Yamane <henrich@debian.org>, 2010-2023
# hoxp18 <hoxp18@noramail.jp>, 2019.
# This file is distributed under the same license as Debian release notes.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 12\n"
"Report-Msgid-Bugs-To: debian-doc@lists.debian.org\n"
"POT-Creation-Date: 2023-05-30 11:48+0900\n"
"PO-Revision-Date: 2023-05-28 18:57+0900\n"
"Last-Translator: Hideki Yamane <henrich@debian.org>\n"
"Language-Team: Japanese <debian-doc@debian.or.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#. type: Attribute 'lang' of: <chapter>
#: en/whats-new.dbk:8
msgid "en"
msgstr "ja"

#. type: Content of: <chapter><title>
#: en/whats-new.dbk:9
msgid "What's new in &debian; &release;"
msgstr "&debian; &release; の最新情報"

#. type: Content of: <chapter><para>
#: en/whats-new.dbk:11
msgid ""
"The <ulink url=\"&url-wiki-newinrelease;\">Wiki</ulink> has more information "
"about this topic."
msgstr ""
"この章のより詳しい情報は <ulink url=\"&url-wiki-newinrelease;\">Wiki</ulink> "
"を参照してください。"

#. type: Content of: <chapter><section><title>
#: en/whats-new.dbk:22
msgid "Supported architectures"
msgstr "サポートするアーキテクチャ"

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:25
msgid ""
"The following are the officially supported architectures for &debian; "
"&release;:"
msgstr ""
"&debian; &releasename; で公式にサポートされているアーキテクチャは以下のとおり"
"です。"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:31
msgid ""
"32-bit PC (<literal>i386</literal>) and 64-bit PC (<literal>amd64</literal>)"
msgstr ""
"32 ビット PC (<literal>i386</literal>) および 64 ビット PC (<literal>amd64</"
"literal>)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:36
msgid "64-bit ARM (<literal>arm64</literal>)"
msgstr "64 ビット ARM (<literal>arm64</literal>)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:41
msgid "ARM EABI (<literal>armel</literal>)"
msgstr "ARM EABI (<literal>armel</literal>)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:46
msgid "ARMv7 (EABI hard-float ABI, <literal>armhf</literal>)"
msgstr "ARMv7 (EABI 浮動小数点ハードウェア ABI, <literal>armhf</literal>)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:51
msgid "little-endian MIPS (<literal>mipsel</literal>)"
msgstr "リトルエンディアン MIPS (<literal>mipsel</literal>)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:56
msgid "64-bit little-endian MIPS (<literal>mips64el</literal>)"
msgstr "64 ビットリトルエンディアン MIPS (<literal>mips64el</literal>)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:61
msgid "64-bit little-endian PowerPC (<literal>ppc64el</literal>)"
msgstr "64 ビットリトルエンディアン PowerPC (<literal>ppc64el</literal>)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:66
msgid "IBM System z (<literal>s390x</literal>)"
msgstr "IBM System z (<literal>s390x</literal>)"

#. type: Content of: <chapter><section><variablelist><varlistentry><term>
#: en/whats-new.dbk:73
msgid "Baseline bump for &arch-title; to <phrase>i686</phrase>"
msgstr "&arch-title; の最低ラインが <phrase>i686</phrase> に引き上げられました"

#. type: Content of: <chapter><section><variablelist><varlistentry><listitem><para>
#: en/whats-new.dbk:77
msgid ""
"The &arch-title; support (known as the Debian architecture &architecture;) "
"now requires the \"long NOP\" instruction. Please refer to <xref "
"linkend=\"i386-is-i686\"/> for more information."
msgstr ""
"(Debian での &architecture; アーキテクチャとして知られる) &arch-title; をサ"
"ポートするには \"long NOP\" 命令を含むことが必要となりました。詳細については "
"<xref linkend=\"i386-is-i686\"/> を参照してください。"

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:87
msgid ""
"You can read more about port status, and port-specific information for your "
"architecture at the <ulink url=\"&url-ports;\">Debian port web pages</ulink>."
msgstr ""
"移植状況の詳細や、お使いの移植版に特有の情報については、<ulink url=\"&url-"
"ports;\">Debian の移植版に関するウェブページ</ulink>で読むことができます。"

#. type: Content of: <chapter><section><title>
#: en/whats-new.dbk:94
msgid "Archive areas"
msgstr "アーカイブエリア"

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:97
msgid ""
"The following archive areas, mentioned in the Social Contract and in the "
"Debian Policy, have been around for a long time:"
msgstr ""
"以下の、社会契約と Debian ポリシーで言及されているアーカイブエリアが長期間に"
"渡って存在しています:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:103
msgid "main: the Debian distribution;"
msgstr "main: Debian ディストリビューションそのもの"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:108
msgid ""
"contrib: supplemental packages intended to work with the Debian "
"distribution, but which require software outside of the distribution to "
"either build or function;"
msgstr ""
"contrib: Debian ディストリビューションと互換性がある補助パッケージ群だが、ビ"
"ルドまたは動作にディストリビューション外のソフトウェアを必要とする"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:115
msgid ""
"non-free: supplemental packages intended to work with the Debian "
"distribution that do not comply with the DFSG or have other problems that "
"make their distribution problematic."
msgstr ""
"non-free: DFSG (Debian フリーソフトウェアガイドライン) に適合しない、あるいは"
"配布を難しくする他の問題があるが、Debian ディストリビューションと互換性がある"
"補助パッケージ群"

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:123
msgid ""
"Following the <ulink url=\"https://www.debian.org/vote/2022/vote_003\">2022 "
"General Resolution about non-free firmware</ulink>, the 5th point of the "
"Social Contract was extended with the following sentence:"
msgstr ""
"<ulink url=\"https://www.debian.org/vote/2022/vote_003\">non-free なファーム"
"ウェアに関する2022年の一般決議</ulink>によって、社会契約の第5条へ以下の文章が"
"追加されました:"

#. type: Content of: <chapter><section><blockquote><para>
#: en/whats-new.dbk:129
msgid ""
"The Debian official media may include firmware that is otherwise not part of "
"the Debian system to enable use of Debian with hardware that requires such "
"firmware."
msgstr ""
"Debian の公式メディアには、Debian システムの一部ではないファームウェアが含ま"
"れていることがあります。これはそのようなファームウェアを必要とするハードウェ"
"アで Debian を利用可能とするのに他の方法がないためです。"

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:136
msgid ""
"While it's not mentioned explicitly in either the Social Contract or Debian "
"Policy yet, a new archive area was introduced, making it possible to "
"separate non-free firmware from the other non-free packages:"
msgstr ""
"未だ社会契約や Debian ポリシーには明示的に反映されていませんが、新しいアーカ"
"イブエリアが導入されたことで、non-free なファームウェアを他の non-free なパッ"
"ケージから隔離できています。"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:145
msgid "non-free-firmware"
msgstr "non-free-firmware"

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:151
msgid ""
"Most non-free firmware packages have been moved from <literal>non-free</"
"literal> to <literal>non-free-firmware</literal> in preparation for the "
"&debian; &release; release.  This clean separation makes it possible to "
"build official installation images with packages from <literal>main</"
"literal> and from <literal>non-free-firmware</literal>, without "
"<literal>contrib</literal> or <literal>non-free</literal>. In turn, these "
"installation images make it possible to install systems with only "
"<literal>main</literal> and <literal>non-free-firmware</literal>, without "
"<literal>contrib</literal> or <literal>non-free</literal>."
msgstr ""
"ほとんどの non-free なファームウェアパッケージが &debian; &release; リリース"
"の準備中に <literal>non-free</literal> から <literal>non-free-firmware</"
"literal> へ移動されました。このクリーンな分離によって <literal>contrib</"
"literal> や <literal>non-free</literal> を利用しない、<literal>main</"
"literal> および <literal>non-free-firmware</literal> のパッケージを利用した公"
"式インストールイメージのビルドが可能になりました。同様に、これらのインストー"
"ルイメージで <literal>contrib</literal> や <literal>non-free</literal> 無し"
"で <literal>main</literal> および <literal>non-free-firmware</literal> のみで"
"システムをインストール可能となっています。"

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:161
msgid ""
"See <xref linkend=\"non-free-firmware\"/> for upgrades from &oldreleasename;."
msgstr ""
"&oldreleasename; からのアップグレードについては <xref linkend=\"non-free-"
"firmware\"/> を参照してください。"

#. type: Content of: <chapter><section><title>
#: en/whats-new.dbk:166
msgid "What's new in the distribution?"
msgstr "ディストリビューションの最新情報"

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:176
msgid ""
"This new release of Debian again comes with a lot more software than its "
"predecessor &oldreleasename;; the distribution includes over &packages-new; "
"new packages, for a total of over &packages-total; packages.  Most of the "
"software in the distribution has been updated: over &packages-updated; "
"software packages (this is &packages-update-percent;% of all packages in "
"&oldreleasename;).  Also, a significant number of packages (over &packages-"
"removed;, &packages-removed-percent;% of the packages in &oldreleasename;) "
"have for various reasons been removed from the distribution.  You will not "
"see any updates for these packages and they will be marked as \"obsolete\" "
"in package management front-ends; see <xref linkend=\"obsolete\"/>."
msgstr ""
"Debian のこの新しいリリースには、一つ前のリリースである &oldreleasename; に含"
"まれていたよりさらに多くのソフトウェアが含まれています。このディストリビュー"
"ションには、&packages-new; 以上の新しいパッケージが含まれており、全体のパッ"
"ケージ数は &packages-total; 以上になりました。ディストリビューション中のほと"
"んどのソフトウェア、すなわち約 &packages-updated; ものソフトウェアパッケージ "
"(これは &oldreleasename; のパッケージ全体の &packages-update-percent;% にあた"
"ります) が更新されました。また、かなりの数のパッケージ (&oldreleasename; の"
"パッケージの &packages-removed-percent;% にあたる &packages-removed; 以上) "
"が、様々な理由でディストリビューションから取り除かれました。これらのパッケー"
"ジは更新されることはなく、パッケージ管理用のフロントエンドでは 'obsolete' と"
"いうマークが付けられます。これについては <xref linkend=\"obsolete\"/> を参照"
"してください。"

#. type: Content of: <chapter><section><section><title>
#: en/whats-new.dbk:191
msgid "Desktops and well known packages"
msgstr "デスクトップとよく知られているパッケージ"

#. type: Content of: <chapter><section><section><para>
#: en/whats-new.dbk:193
msgid ""
"&debian; again ships with several desktop applications and environments.  "
"Among others it now includes the desktop environments "
"GNOME<indexterm><primary>GNOME</primary></indexterm> 43, KDE "
"Plasma<indexterm><primary>KDE</primary></indexterm> 5.27, "
"LXDE<indexterm><primary>LXDE</primary></indexterm> 11, "
"LXQt<indexterm><primary>LXQt</primary></indexterm> 1.2.0, "
"MATE<indexterm><primary>MATE</primary></indexterm> 1.26, and "
"Xfce<indexterm><primary>Xfce</primary></indexterm> 4.18."
msgstr ""
"&debian; は今回も複数のデスクトップアプリケーションとデスクトップ環境をサポー"
"トしています。中でも GNOME<indexterm><primary>GNOME</primary></indexterm> "
"43, KDE Plasma<indexterm><primary>KDE</primary></indexterm> 5.27, "
"LXDE<indexterm><primary>LXDE</primary></indexterm> 11, "
"LXQt<indexterm><primary>LXQt</primary></indexterm> 1.2.0, "
"MATE<indexterm><primary>MATE</primary></indexterm> 1.26, "
"Xfce<indexterm><primary>Xfce</primary></indexterm> 4.18 があります。"

#. type: Content of: <chapter><section><section><para>
#: en/whats-new.dbk:203
msgid ""
"Productivity applications have also been upgraded, including the office "
"suites:"
msgstr ""
"事務用アプリケーションもオフィススイートを含めてアップグレードされています:"

#. type: Content of: <chapter><section><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:209
msgid ""
"LibreOffice<indexterm><primary>LibreOffice</primary></indexterm> is upgraded "
"to version 7.4;"
msgstr ""
"LibreOffice<indexterm><primary>LibreOffice</primary></indexterm> が 7.4 へ"
"アップグレードされました。"

#. type: Content of: <chapter><section><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:215
msgid ""
"GNUcash<indexterm><primary>GNUcash</primary></indexterm> is upgraded to 4.13;"
msgstr ""
"GNUcash<indexterm><primary>GNUcash</primary></indexterm> が 4.13 へアップグ"
"レードされました。"

#. type: Content of: <chapter><section><section><para>
#: en/whats-new.dbk:242
msgid ""
"Among many others, this release also includes the following software updates:"
msgstr ""
"またこのリリースには、特に挙げるなら、以下のソフトウェアの更新も含まれていま"
"す:"

#. type: Content of: <chapter><section><section><informaltable><tgroup><thead><row><entry>
#: en/whats-new.dbk:252
msgid "Package"
msgstr "パッケージ"

#. type: Content of: <chapter><section><section><informaltable><tgroup><thead><row><entry>
#: en/whats-new.dbk:253
msgid "Version in &oldrelease; (&oldreleasename;)"
msgstr "&oldrelease; (&oldreleasename;) でのバージョン"

#. type: Content of: <chapter><section><section><informaltable><tgroup><thead><row><entry>
#: en/whats-new.dbk:254
msgid "Version in &release; (&releasename;)"
msgstr "&release; (&releasename;) でのバージョン"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:259
msgid "Apache<indexterm><primary>Apache</primary></indexterm>"
msgstr "Apache<indexterm><primary>Apache</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:260
msgid "2.4.54"
msgstr "2.4.54"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:261 en/whats-new.dbk:366
msgid "2.4.57"
msgstr "2.4.57"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:264
msgid ""
"BIND<indexterm><primary>BIND</primary></indexterm> <acronym>DNS</acronym> "
"Server"
msgstr ""
"BIND<indexterm><primary>BIND</primary></indexterm> <acronym>DNS</acronym> "
"サーバ"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:265
msgid "9.16"
msgstr "9.16"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:266
msgid "9.18"
msgstr "9.18"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:281
msgid "Cryptsetup<indexterm><primary>Cryptsetup</primary></indexterm>"
msgstr "Cryptsetup<indexterm><primary>Cryptsetup</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:282
msgid "2.3"
msgstr "2.3"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:283
msgid "2.6"
msgstr "2.6"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:293
msgid ""
"Dovecot<indexterm><primary>Dovecot</primary></indexterm> <acronym>MTA</"
"acronym>"
msgstr ""
"Dovecot<indexterm><primary>Dovecot</primary></indexterm> <acronym>MTA</"
"acronym>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:294
msgid "2.3.13"
msgstr "2.3.13"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:295
msgid "2.3.19"
msgstr "2.3.19"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:298
msgid "Emacs"
msgstr "Emacs"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:299
msgid "27.1"
msgstr "27.1"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:300
msgid "28.2"
msgstr "28.2"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:303
msgid ""
"Exim<indexterm><primary>Exim</primary></indexterm> default e-mail server"
msgstr ""
"Exim<indexterm><primary>Exim</primary></indexterm> 標準の電子メールサーバ"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:304
msgid "4.94"
msgstr "4.94"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:305
msgid "4.96"
msgstr "4.96"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:315
msgid ""
"<acronym>GNU</acronym> Compiler Collection as default "
"compiler<indexterm><primary>GCC</primary></indexterm>"
msgstr ""
"<acronym>GNU</acronym> Compiler Collection (デフォルトのコンパイ"
"ラ)<indexterm><primary>GCC</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:316
msgid "10.2"
msgstr "10.2"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:317
msgid "12.2"
msgstr "12.2"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:320
msgid "<acronym>GIMP</acronym><indexterm><primary>GIMP</primary></indexterm>"
msgstr "<acronym>GIMP</acronym><indexterm><primary>GIMP</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:321
msgid "2.10.22"
msgstr "2.10.22"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:322
msgid "2.10.34"
msgstr "2.10.34"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:325
msgid "GnuPG<indexterm><primary>GnuPG</primary></indexterm>"
msgstr "GnuPG<indexterm><primary>GnuPG</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:326
msgid "2.2.27"
msgstr "2.2.27"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:327
msgid "2.2.40"
msgstr "2.2.40"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:330
msgid "Inkscape<indexterm><primary>Inkscape</primary></indexterm>"
msgstr "Inkscape<indexterm><primary>Inkscape</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:331
msgid "1.0.2"
msgstr "1.0.2"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:332
msgid "1.2.2"
msgstr "1.2.2"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:335
msgid "the <acronym>GNU</acronym> C library"
msgstr "<acronym>GNU</acronym> C ライブラリ"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:336
msgid "2.31"
msgstr "2.31"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:337
msgid "2.36"
msgstr "2.36"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:340
msgid "lighttpd"
msgstr "lighttpd"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:341
msgid "1.4.59"
msgstr "1.4.59"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:342
msgid "1.4.69"
msgstr "1.4.69"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:345
msgid "Linux kernel image"
msgstr "Linux カーネルイメージ"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:346
msgid "5.10 series"
msgstr "5.10 シリーズ"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:347
msgid "6.1 series"
msgstr "6.2 シリーズ"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:350
msgid "LLVM/Clang toolchain"
msgstr "LLVM/Clang ツールチェイン"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:351
msgid "9.0.1 and 11.0.1 (default) and 13.0.1"
msgstr "9.0.1, 11.0.1 (デフォルト) そして 13.0.1"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:352
msgid "13.0.1 and 14.0 (default) and 15.0.6"
msgstr "13.0.1, 14.0 (デフォルト) そして 15.0.6"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:355
msgid "MariaDB<indexterm><primary>MariaDB</primary></indexterm>"
msgstr "MariaDB<indexterm><primary>MariaDB</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:356
msgid "10.5"
msgstr "10.5"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:357
msgid "10.11"
msgstr "10.11"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:360
msgid "Nginx<indexterm><primary>Nginx</primary></indexterm>"
msgstr "Nginx<indexterm><primary>Nginx</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:361
msgid "1.18"
msgstr "1.18"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:362
msgid "1.22"
msgstr "1.22"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:365
msgid "OpenLDAP"
msgstr "OpenLDAP"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:367
msgid "2.5.13"
msgstr "2.5.13"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:370
msgid "OpenJDK<indexterm><primary>OpenJDK</primary></indexterm>"
msgstr "OpenJDK<indexterm><primary>OpenJDK</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:371
msgid "11"
msgstr "11"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:372
msgid "17"
msgstr "17"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:375
msgid "OpenSSH<indexterm><primary>OpenSSH</primary></indexterm>"
msgstr "OpenSSH<indexterm><primary>OpenSSH</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:376
msgid "8.4p1"
msgstr "8.4p1"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:377
msgid "9.2p1"
msgstr "9.2p1"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:380
msgid "Perl<indexterm><primary>Perl</primary></indexterm>"
msgstr "Perl<indexterm><primary>Perl</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:381
msgid "5.32"
msgstr "5.32"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:382
msgid "5.36"
msgstr "5.36"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:385
msgid "<acronym>PHP</acronym><indexterm><primary>PHP</primary></indexterm>"
msgstr "<acronym>PHP</acronym><indexterm><primary>PHP</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:386
msgid "7.4"
msgstr "7.4"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:387 en/whats-new.dbk:428
msgid "8.2"
msgstr "8.2"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:390
msgid ""
"Postfix<indexterm><primary>Postfix</primary></indexterm> <acronym>MTA</"
"acronym>"
msgstr ""
"Postfix<indexterm><primary>Postfix</primary></indexterm> <acronym>MTA</"
"acronym>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:391
msgid "3.5"
msgstr "3.5"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:392
msgid "3.7"
msgstr "3.7"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:395
msgid "PostgreSQL<indexterm><primary>PostgreSQL</primary></indexterm>"
msgstr "PostgreSQL<indexterm><primary>PostgreSQL</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:396
msgid "13"
msgstr "13"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:397
msgid "15"
msgstr "15"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:407
msgid "Python 3"
msgstr "Python 3"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:408
msgid "3.9.2"
msgstr "3.9.2"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:409
msgid "3.11.2"
msgstr "3.11.2"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:412
msgid "Rustc"
msgstr "Rustc"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:413
msgid "1.48"
msgstr "1.48"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:414
msgid "1.63"
msgstr "1.63"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:417
msgid "Samba"
msgstr "Samba"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:418
msgid "4.13"
msgstr "4.13"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:419
msgid "4.17"
msgstr "4.17"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:422
msgid "systemd"
msgstr "systemd"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:423
msgid "247"
msgstr "247"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:424
msgid "252"
msgstr "252"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:427
msgid "Vim"
msgstr "Vim"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:429
msgid "9.0"
msgstr "9.0"

#. type: Content of: <chapter><section><section><title>
#: en/whats-new.dbk:438
msgid "More translated man pages"
msgstr "さらに翻訳された manpage"

#. type: Content of: <chapter><section><section><para>
#: en/whats-new.dbk:440
msgid ""
"Thanks to our translators, more documentation in <command>man</command>-page "
"format is available in more languages than ever. For example, many man pages "
"are now available in Czech, Danish, Greek, Finnish, Indonesian, Macedonian, "
"Norwegian (Bokmål), Russian, Serbian, Swedish, Ukrainian and Vietnamese, and "
"all <systemitem role=\"package\">systemd</systemitem> man pages are now "
"available in German."
msgstr ""
"翻訳者らのおかげで、<command>man</command>-page 形式でのさらに多くのドキュメ"
"ントがこれまでより多くの言語で利用できるようになっています。例として、多くの "
"manpage がチェコ語・デンマーク語・ギリシャ語・フィンランド語・インドネシア"
"語・マケドニア語・ノルウェー語 (ブークモール)・ロシア語・セルビア語・スウェー"
"デン語・ウクライナ語・ベトナム語で利用できるようになり、さらに <systemitem "
"role=\"package\">systemd</systemitem> の全ての manpage がドイツ語になっていま"
"す。"

#. type: Content of: <chapter><section><section><para>
#: en/whats-new.dbk:450
msgid ""
"To ensure the <command>man</command> command shows the documentation in your "
"language (where possible), install the right <systemitem "
"role=\"package\">manpages-<replaceable>lang</replaceable></systemitem> "
"package and make sure your locale is correctly configured by using"
msgstr ""
"<command>man</command> コマンドが (可能な場合に) あなたの言語でドキュメントを"
"表示するようにするには、適切な <systemitem role=\"package\">manpages-"
"<replaceable>lang</replaceable></systemitem> パッケージをインストールし、以下"
"のコマンドでロケールを正しく設定してください"

#. type: Content of: <chapter><section><section><para><programlisting>
#: en/whats-new.dbk:456
#, no-wrap
msgid "dpkg-reconfigure locales"
msgstr "dpkg-reconfigure locales"

#. type: Content of: <chapter><section><section><para>
#: en/whats-new.dbk:456
msgid "."
msgstr "."

#. type: Content of: <chapter><section><section><title>
#: en/whats-new.dbk:461
msgid "News from Debian Med Blend"
msgstr "Debian Med Blend からのニュース"

#. type: Content of: <chapter><section><section><para>
#: en/whats-new.dbk:463
msgid ""
"As in every release new packages have been added in the fields of medicine "
"and life sciences. The new package <systemitem role=\"package\">shiny-"
"server</systemitem> might be worth a particular mention, since it simplifies "
"scientific web applications using <literal>R</literal>. We also kept up the "
"effort to provide Continuous Integration support for the packages maintained "
"by the Debian Med team."
msgstr ""
"リリースごとに医療・ライフサイエンス分野の新規パッケージが追加されています。"
"新パッケージ <systemitem role=\"package\">shiny-server</systemitem> は "
"<literal>R</literal> を使った科学向けウェブアプリケーションを簡易に作成してく"
"れるので特に言及する価値があるでしょう。また、我々は Debian Med team がメンテ"
"ナンスしているパッケージ群へ継続的インテグレーション (CI) サポートの提供に向"
"けた努力を続けています。"

#. type: Content of: <chapter><section><section><para>
#: en/whats-new.dbk:472
msgid ""
"The Debian Med team is always interested in feedback from users, especially "
"in the form of requests for packaging of not-yet-packaged free software, or "
"for backports from new packages or higher versions in testing."
msgstr ""
"Debian Med team は常にユーザーからのフィードバックに興味を持っています。パッ"
"ケージ化されていないフリーソフトウェアのパッケージ化リクエストや、testing に"
"ある新規パッケージやより新しいバージョンのパッケージを backports へ投入するこ"
"とについては特にです。"

#. type: Content of: <chapter><section><section><para>
#: en/whats-new.dbk:478
msgid ""
"To install packages maintained by the Debian Med team, install the "
"metapackages named <literal>med-<replaceable>*</replaceable></literal>, "
"which are at version 3.8.x for Debian bookworm. Feel free to visit the "
"<ulink url=\"https://blends.debian.org/med/tasks\">Debian Med tasks pages</"
"ulink> to see the full range of biological and medical software available in "
"Debian."
msgstr ""
"Debian Med team がメンテナンスしているパッケージをインストールするには、"
"<literal>med-<replaceable>*</replaceable></literal> という名前の Debian "
"bookworm ではバージョン 3.8.x のメタパッケージをインストールしてください。"
"Debian で入手可能な全範囲の生物・医療関連ソフトウェアを参照するには <ulink "
"url=\"https://blends.debian.org/med/tasks\">Debian Med tasks pages</ulink> に"
"お気軽にお越しください。"

#. type: Content of: <chapter><section><section><title>
#: en/whats-new.dbk:489
msgid "Something"
msgstr "Something"

#. type: Content of: <chapter><section><section><para>
#: en/whats-new.dbk:491
msgid "Text"
msgstr "Text"

#, no-wrap
#~ msgid ""
#~ "      TODO: Make sure you update the numbers in the .ent file\n"
#~ "      using the changes-release.pl script found under ../\n"
#~ "    "
#~ msgstr ""
#~ "      TODO: Make sure you update the numbers in the .ent file\n"
#~ "      using the changes-release.pl script found under ../\n"
#~ "    "

#~ msgid "2.4.56"
#~ msgstr "2.4.56"
