# Copyright (C) 2020 Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
#
# galaxico <galas@tee.gr>, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2021-08-28 01:54+0300\n"
"PO-Revision-Date: 2023-05-29 20:04+0300\n"
"Last-Translator: galaxico <galas@tee.gr>\n"
"Language-Team: debian-l10n-greek@lists.debian.org\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#. type: Attribute 'lang' of: <chapter>
#: en/installing.dbk:8
msgid "en"
msgstr "el"

#. type: Content of: <chapter><title>
#: en/installing.dbk:9
msgid "Installation System"
msgstr "Σύστημα Εγκατάστασης"

#. type: Content of: <chapter><para>
#: en/installing.dbk:11
msgid ""
"The Debian Installer is the official installation system for Debian.  It "
"offers a variety of installation methods.  Which methods are available to "
"install your system depends on your architecture."
msgstr ""
"Ο Εγκαταστάτης του Debian είναι το επίσημο σύστημα εγκατάστασης για το "
"Debian. Προσφέρει μια ποικιλία μεθόδων εγκατάστασης. Ποιες μέθοδοι είναι "
"διαθέσιμες για εγκατάσταση στο σύστημά σας εξαρτάται από την αρχιτεκτονική "
"του."

#. type: Content of: <chapter><para>
#: en/installing.dbk:16
msgid ""
"Images of the installer for &releasename; can be found together with the "
"Installation Guide on the <ulink url=\"&url-installer;\">Debian website</"
"ulink>."
msgstr ""
"Εικόνες του εγκαταστάτη για την έκδοση &releasename; μπορούν να βρεθούν μαζί "
"με τον Οδηγό Εγκατάστασης στον σύνδεσμο <ulink url=\"&url-installer;"
"\">Ιστότοπος του Debian </ulink>."

#. type: Content of: <chapter><para>
#: en/installing.dbk:21
msgid ""
"The Installation Guide is also included on the first media of the official "
"Debian DVD (CD/blu-ray) sets, at:"
msgstr ""
"Ο Οδηγός Εγκατάστασης βρίσκεται επίσης στο πρώτο από τα μέσα του επίσημου "
"σετ Debian DVD (CD/blu-ray) στο:"

#. type: Content of: <chapter><screen>
#: en/installing.dbk:25
#, no-wrap
msgid "/doc/install/manual/<replaceable>language</replaceable>/index.html\n"
msgstr "/doc/install/manual/<replaceable>language</replaceable>/index.html\n"

#. type: Content of: <chapter><para>
#: en/installing.dbk:28
msgid ""
"You may also want to check the <ulink url=\"&url-installer;index#errata"
"\">errata</ulink> for debian-installer for a list of known issues."
msgstr ""
"Πιθανόν να θέλετε να ελέγξετε επίσης τον σύνδεσμο <ulink url=\"&url-"
"installer;index#errata\">errata</ulink> του debian-installer για μια λίστα "
"γνωστών ζητημάτων."

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:33
msgid "What's new in the installation system?"
msgstr "Τι είναι καινούριο στο σύστημα εγκατάστασης;"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:35
msgid ""
"There has been a lot of development on the Debian Installer since its "
"previous official release with &debian; &oldrelease;, resulting in improved "
"hardware support and some exciting new features or improvements."
msgstr ""
"Έχει γίνει αρκετή ανάπτυξη στον Εγκαταστάτη του Debian από την προηγούμενη "
"επίσημη έκδοσή του με το &debian; &oldrelease;, που έχει ως αποτέλεσμα "
"βελτιωμένη υποστήριξη υλικού και μερικά συναρπαστικά καινούρια "
"χαρακτηριστικά ή βελτιώσεις."

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:41
msgid ""
"If you are interested in an overview of the detailed changes since "
"&oldreleasename;, please check the release announcements for the "
"&releasename; beta and RC releases available from the Debian Installer's "
"<ulink url=\"&url-installer-news;\">news history</ulink>."
msgstr ""
"Αν ενδιαφέρεστε για μια επισκόπηση των λεπτομερών αλλαγών από την "
"έκδοση&oldreleasename;, παρακαλούμε ελέγξτε τις ανακοινώσεις της έκδοσεων "
"beta και RC της &releasename; που είναι διαθέσιμες από τον σύνδεσμο του "
"Εγκαταστάτη του Debian<ulink url=\"&url-installer-news;\">news history</"
"ulink>."

#. type: Content of: <chapter><section><section><title>
#: en/installing.dbk:52
msgid "Help with installation of firmware"
msgstr "Βοήθεια με την εγκατάσταση υλισμικού"

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:54
msgid ""
"More and more, peripheral devices require firmware to be loaded as part of "
"the hardware initialization. To help deal with this problem, the installer "
"has a new feature. If some of the installed hardware requires firmware files "
"to be installed, the installer will try to add them to the system, based on "
"a mapping from hardware ID to firmware file names."
msgstr ""
"Όλο και περισσότερες περιφερειακές συσκευές απαιτούν την φόρτωση "
"υλισμικού ως μέρους της αρχικοποίησης του υλικού. Για να βοηθήσει "
"με αυτό το πρόβλημα, ο εγκαταστάτης έχει ένα καινούριο γνώρισμα. "
"Αν κάποιο από το εγκατεστημένο υλικό χρειάζεται την εγκατάσταση "
"αρχείων υλισμικού, ο εγκαταστάτης θα προσπαθήσει να τα προσθέσει στο "
"σύστημα με βάση μια αντιστοίχιση ID υλικού με ονόματα αρχείων υλισμικού. "

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:62
msgid ""
"This new functionality is restricted to the unofficial installer images with "
"firmware included (see <ulink url=\"&url-installer;#firmware_nonfree\">&url-"
"installer;#firmware_nonfree</ulink>).  The firmware is usually not DFSG "
"compliant, so it is not possible to distribute it in Debian's main "
"repository."
msgstr ""
"Αυτή η νέα λειτουργικότητα περιορίζεται στις ανεπίσημες "
"εικόνες του εγκαταστάτη που περιλαμβάνουν και υλισμικό "
"(δείτε την σελίδα <ulink url=\"&url-installer;#firmware_nonfree\">&url-"
"installer;#firmware_nonfree</ulink>). Το υλισμικό είναι συνήθως μη συμβατό "
"με τις καθοδηγητικές γραμμές του DFSG, οπότε δεν είναι εφικτή η διανομή "
"του από το κύριο αποθετήριο του Debian."

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:69
msgid ""
"If you experience problems related to (missing) firmware, please read <ulink "
"url=\"https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-"
"installed-system\">the dedicated chapter of the installation-guide</ulink>."
msgstr ""
"Αν συναντήσετε προβλήματα με σχετιζόμενο υλισμικό (που απουσιάζει),"
" παρακαλούμε "
"διαβάστε το <ulink "
"url=\"https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-"
"installed-system\">σχετικά αφιερωμένο κεφάλαιο στον Οδηγό εγκατάστασης</ulink"
">."

#. type: Content of: <chapter><section><section><title>
#: en/installing.dbk:146
msgid "Automated installation"
msgstr "Αυτοματοποιημένη Εγκατάσταση"

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:148
#| msgid ""
#| "Some changes mentioned in the previous section also imply changes in the "
#| "support in the installer for automated installation using "
#| "preconfiguration files.  This means that if you have existing "
#| "preconfiguration files that worked with the &oldreleasename; installer, "
#| "you cannot expect these to work with the new installer without "
#| "modification."
msgid ""
"Some changes also imply changes in the support in the installer for "
"automated installation using preconfiguration files.  This means that if you "
"have existing preconfiguration files that worked with the &oldreleasename; "
"installer, you cannot expect these to work with the new installer without "
"modification."
msgstr ""
"ερικές αλλαγές συνεπάγονται επίσης αλλαγές στην υποστήριξη από τον"
" εγκαταστάτη της αυτοματοποιημένης εγκατάστασης με τη χρήση αρχείων"
" προρρύθμισης. Αυτό σημαίνει ότι αν έχετε κάποια υπάρχοντα αρχεία"
" προρρυθμίσεων που δούλευαν με τον εγκαταστάτη της "
"έκδοσης  &oldreleasename;, δεν θα πρέπει να περιμένετε ότι θα δουλέψουν και "
"με τον καινούριο εγκαταστάτη χωρίς τροποποιήσεις."

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:155
msgid ""
"The <ulink url=\"&url-install-manual;\">Installation Guide</ulink> has an "
"updated separate appendix with extensive documentation on using "
"preconfiguration."
msgstr ""
"Ο σύνδεσμος <ulink url=\"&url-install-manual;\">Οδηγός Εγκατάστασης</ulink> "
"διαθέτει ένα ξεχωριστό επικαιροποιημένο προσάρτημα με εκτενή τεκμηρίωση για "
"την χρήση προρρυθμίσεων."

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:164
msgid "Cloud installations"
msgstr "Εγκαταστάσεις στο Νέφος"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:166
msgid ""
"The <ulink url=\"&url-cloud-team;\">cloud team</ulink> publishes Debian "
"bullseye for several popular cloud computing services including:"
msgstr ""
"Η <ulink url=\"&url-cloud-team;\">ομάδα Νέφους </ulink> εκδίδει το Debian "
"bullseye για αρκετές δημοφιλείς υπηρεσίες υπολογιστικού Νέφους "
"περιλαμβανομένων των:"

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:173
msgid "OpenStack"
msgstr "OpenStack"

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:178
msgid "Amazon Web Services"
msgstr "Amazon Web Services"

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:183
msgid "Microsoft Azure"
msgstr "Microsoft Azure"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:190
msgid ""
"Cloud images provide automation hooks via cloud-init and prioritize fast "
"instance startup using specifically optimized kernel packages and grub "
"configurations.  Images supporting different architectures are provided "
"where appropriate and the cloud team endeavors to support all features "
"offered by the cloud service."
msgstr ""
"Οι εικόνες για Νέφος παρέχουν hooks αυτοματοποίησης μέσω του cloud-init και "
"δίνουν προτεραιότητα σε ταχείες εκκινήσεις συστημάτων χρησιμοποιώντας "
"βελτιστοποιημένα πακέτα πυρήνα και ρυθμίσεις του grub. Παρέχονται εικόνες "
"που υποστηρίζουν διαφορετικές αρχιτεκτονικές όπου είναι κατάλληλο και η "
"ομάδα Νέφους προσπαθεί να υποστηρίξει όλα τα χαρακτηριστικά που προσφέρονται "
"από την υπηρεσία Νέφους."

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:199
msgid ""
"The cloud team will provide updated images until the end of the LTS period "
"for bullseye.  New images are typically released for each point release and "
"after security fixes for critical packages.  The cloud team's full support "
"policy can be found <ulink url=\"&url-cloud-wiki-imagelifecycle;\">here</"
"ulink>."
msgstr ""
"Η ομάδα Νέφους θα προσφέρει επικαιροποιημένες εικόνες μέχρι το τέλος "
"της περιόδου LTS για την έκδοση bullseye. Καινούριες εικόνες κυκλοφορούν "
"τυπικά για κάθε σημειακή έκδοση και μετά από διορθώσεις ασφαλείας για "
"κρίσιμα πακέτα. Η πολιτική πλήρους υποστήριξης της ομάδας Νέφους μπορεί "
"να βρεθεί <ulink url=\"&url-cloud-wiki-imagelifecycle;\">εδώ</"
"ulink>."

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:207
msgid ""
"More details are available at <ulink url=\"&url-cloud;\">cloud.debian.org</"
"ulink> and <ulink url=\"&url-cloud-wiki;\">on the wiki</ulink>."
msgstr ""
"Περισσότερες πληροφορίες είναι διαθέσιμες στον σύνδεσμο  <ulink url=\"&url-"
"cloud;\">cloud.debian.org</ulink> και στη σελίδα <ulink url=\"&url-cloud-"
"wiki;\">στο wiki</ulink>."

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:214
msgid "Container and Virtual Machine images"
msgstr "Εικόνες για Container και Εικονικές Μηχανές"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:216
msgid ""
"Multi-architecture Debian bullseye container images are available on <ulink "
"url=\"&url-docker-hub;\">Docker Hub</ulink>.  In addition to the standard "
"images, a <quote>slim</quote> variant is available that reduces disk usage."
msgstr ""
"Εικόνες πολλαπλών αρχιτεκτονικών του Debian bullseye για container "
"διατίθενται στο <ulink url=\"&url-docker-hub;\">Docker Hub</ulink>. "
"Επιπρόσθετα από τις συνήθεις εικόνες, μια εκδοχή <quote>slim</quote> είναι "
"επίσης διαθέσιμη που μειώνει τη χρήση δίσκου."

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:222
msgid ""
"Virtual machine images for the Hashicorp Vagrant VM manager are published to "
"<ulink url=\"&url-vagrant-cloud;\">Vagrant Cloud</ulink>."
msgstr ""
"Εικόνες για εικονικές μηχανές (VM) για τον διαχειριστή Hashicorp Vagrant "
"είναι διαθέσιμες στον ιστότοπο <ulink url=\"&url-vagrant-cloud;\">Vagrant "
"Cloud</ulink>."

#~ msgid ""
#~ "Most notably there is the initial support for UEFI Secure Boot (see <xref "
#~ "linkend=\"secure-boot\"/>), which has been added to the installation "
#~ "images."
#~ msgstr ""
#~ "Το πιο αξιοσημείωτο είναι η αρχική υποστήριξη για ασφαλή εκκίνηση με το "
#~ "UEFI Secure Boot (see <xref linkend=\"secure-boot\"/>),η οποία έχει "
#~ "προστεθεί στις εικόνες εγκατάστασης."
